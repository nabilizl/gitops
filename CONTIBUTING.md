# Contributing to IntegraCI
This page provides information about contributing code to the IntegraCI Engine.

## Getting started
1. Fork the repository on GitLab
2. Clone the forked repository to your machine
3. You can install Jenkins on local machine or using our machine to test your codebase.

## Testing changes
The library must be have the unit, code review, and functional tests as a part of the repository. 

### Unit test
TBD

### Code review
TBD

### Functionality tests
TBD

## Proposing changes
The IntegraCI project source code repositories are hosted at GitLab. All proposed changes are submitted, and code reviewed, using the GitLab Merge Request process.

To submit a changes:
1. Commit your changes and push them to your fork on GitLab. It is a good practice is to create branches instead of pushing to master.
2. In the GitLab UI, click the New Merge Request button.
3. Select your branch as source branch and master as target branch.
- We integrated all changes into the master branch towards the Weekly releases or asap based on urgency issues.
- After that, the project used our library will be automate updated.
4. Fill in the Merge Request description according to the [proposed template].
5. Click create merge request.
6. Wait for CI results/review, process the feedback.
- If you do not get feedback after 3 days, feel free to ping @integraci/reviewers in the comments.
- Usually we merge requests after 2 approvals from reviewers, no requested changes, and having waited some more time to give others an opportunity to provide their feedback. See this page for more information about our review process.

Once your Merge Request is ready to be merged, the repository maintainers will integrate it, prepare changelogs, and ensure it gets released in one of upcoming Weekly releases. There is no additional action required from merge request authors at this point.

## Idea Suggestions
TBD

## Copyright
TBD

## Continuous Integration
TBD

# Links