# Get Bearer Token
I dont understand about this concern, it's like base64 token. But the quick solution, we can use argocd cli to do it.
- run #argocd cluster add $CONTEXTNAME
- switch to dst cluster, edit the secret #kubectl edit secret -n kube-system argocd-manager-token-xyz
- you can use token to fulfill bearer token, and caData from ca.cert