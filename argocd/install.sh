#!/bin/bash

# Prerequisites
# Install helm v3 before running this script

# Set environment variables
RELEASE_NAME='argocd'
ARGOCD_NS='argocd'
HELM_REPO='./argocd'
VALUES_FILE="./argocd/values.yaml"

# Add ArgoCD Helm Repository
echo "Add helm repo for argocd"
# helm repo add argo https://argoproj.github.io/argo-helm
helm dependency update $HELM_REPO

# Install ArgoCD with Helm Chart
echo -n "Do you want to proceed? [y/n]: "
read ans
if [ "$ans" == "y" ]; then
  helm upgrade -f $VALUES_FILE --install $RELEASE_NAME $HELM_REPO \
    --namespace $ARGOCD_NS \
    --create-namespace
  rm -rf $HELM_REPO/charts
  rm -rf $HELM_REPO/Chart.lock
else
  echo "INFO: Exit without any action"
  exit 0
fi