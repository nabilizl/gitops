#!/bin/bash

# Prerequisites
# Install helm v3 before running this script

# Set environment variables
RELEASE_NAME='argocd'
HELM_REPO='argo/argo-cd'
ARGOCD_NS='argocd'
VALUES_FILE="./installer/argocd/values.yaml"

# Install ArgoCD with Helm Chart
echo -n "Uninstall $RELEASE_NAME in namespace $ARGOCD_NS. Do you want to proceed? [y/n]: "
read ans
if [ "$ans" == "y" ]; then
  helm uninstall $RELEASE_NAME \
    --namespace $ARGOCD_NS
  kubectl proxy &
  kubectl get namespace $ARGOCD_NS -o json |jq '.spec = {"finalizers":[]}' >temp.json
  curl -k -H "Content-Type: application/json" -X PUT --data-binary @temp.json 127.0.0.1:8001/api/v1/namespaces/$ARGOCD_NS/finalize
else
  echo "INFO: Exit without any action"
  exit 0
fi