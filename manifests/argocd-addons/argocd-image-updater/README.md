# Steps
After configure the helm chart, and deployed. you need some configuration below:
1. Edit argocd-cm and add the data here:
   ```
   data:
    #...
    accounts.image-updater: apiKey
   ```
2. Make sure you already login in argocd. Run command below. Copy the result secret in somewhere:
    ```
    argocd account generate-token --account image-updater --id image-updater
    ```
3. Example the result token, add the token in templates
    ```
    eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiJpbWFnZS11cGRhdGVyIiwiaWF0IjoxNjM5Mjk4NDIxLCJpc3MiOiJhcmdvY2QiLCJuYmYiOjE2MzkyOTg0MjEsInN1YiI6ImltYWdlLXVwZGF0ZXI6YXBpS2V5In0.64wzPQbH2Q1cG86bVeH8dJ10Sl_fexO4h5dJ-lzTWqA
    ```
4. Edit rbac argocd/configs/templates/rbac.yaml. add the data below:
    ```
    p, role:image-updater, applications, get, */*, allow
    p, role:image-updater, applications, update, */*, allow
    g, image-updater, role:image-updater
    ```